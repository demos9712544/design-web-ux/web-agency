# Web agency 


This project is part of my portfolio to show my skills in web development. You can clone the project and see my code.

This project has been created with HTML, CSS3 and Javascript.

1. Visual elements created with HTML5.
2. Style sheet with CSS3.
3. Dark mode made with Javascript and CSS to change style and colors.
4. Responsive styles for mobile, tablet made with media query through CSS.
5.Navbar component mobile made with javascript.
6. Fixed menu made with Javascript.

URL DEPLOY Netlify

https://web-agency-ux.netlify.app/